  getAllFormErrors(formGroup: FormGroup): ValidationErrors | null {
    const errors: ValidationErrors = {};

    // Iterate through each control in the form group
    Object.keys(formGroup.controls).forEach((key) => {
      const control = formGroup.get(key);

      // If the control is a FormGroup, recursively call the function
      if (control instanceof FormGroup) {
        const groupErrors = this.getAllFormErrors(control);

        if (groupErrors) {
          errors[key] = groupErrors;
        }
      } else {
        // If the control has errors, add them to the errors object
        if (control?.errors) {
          errors[key] = control.errors;
        }
      }
    });

    // Return the accumulated errors or null if there are no errors
    return Object.keys(errors).length > 0 ? errors : null;
  }
  btn(){
    const allErrors = this.getAllFormErrors(this.peiceJointeForm);

    console.log(allErrors);

  }
